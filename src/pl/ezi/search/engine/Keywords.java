package pl.ezi.search.engine;

import java.util.ArrayList;

import pl.ezi.search.Settings;

public class Keywords extends WordBunch {
	
	ArrayList<String> originalList;
	
	public Keywords(Settings settings) {
		super(settings);
		originalList = new ArrayList<String>();
	}

	@Override
	public void readFromString(String data) {
		list.clear();
		String words[] = data.replaceAll("[^a-zA-Z0-9&\\s]", "").replaceAll("[\\s]+$", "").replaceAll("^[\\s]+", "").toLowerCase().split("[\\s]+");
		for (String word : words) {
			String stemmedWord = stem(word);
			if (!list.contains(stemmedWord)) {
				list.add(stemmedWord);
				originalList.add(word);
			}
		}
		wordBag = new WordBag(list);
	}
	
	public String getOriginalWord(int index) {
		return originalList.get(index);
	}
}
