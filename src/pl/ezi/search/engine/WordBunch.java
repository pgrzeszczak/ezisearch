package pl.ezi.search.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.tartarus.Stemmer;

import pl.ezi.search.Settings;

public class WordBunch {
	ArrayList<String> list;
	Stemmer steemer = new Stemmer();
	WordBag wordBag;
	Settings settings;
	
	public WordBunch(Settings settings)
	{
		list = new ArrayList<String>();
		this.settings = settings;
		wordBag = new WordBag(list);
	}
	
	public boolean readFromFile(File file) {
		String fileContent = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				fileContent += line + "\n";
			}
			readFromString(fileContent);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void readFromString(String data) {
		list.clear();
		String words[] = data.replaceAll("[^a-zA-Z0-9&\\s]", "").replaceAll("[\\s]+$", "").replaceAll("^[\\s]+", "").toLowerCase().split("[\\s]+");
		for (String word : words) {
			list.add(stem(word));
		}
		wordBag = new WordBag(list);
	}
	
	@Override
	public String toString() {
		return StringUtils.join(list, " ");
	}
	
	public String get(int index) {
		return list.get(index);
	}
	
	public int size() {
		return list.size();
	}
	
	public boolean has(String word) {
		return list.contains(word);
	}
	
	public WordBag getWordBag() {
		return wordBag;
	}
	
	public double getWordFrequent(String word) {
		return (double)wordBag.getCount(word)/(double)wordBag.size();
	}
	
	public int findIndex(String word) {
		return list.indexOf(word);
	}
	
	protected String stem(String word) {
		if (!settings.isStemming()) {
			return word;
		}
		char wordCA[] = word.toCharArray();
		steemer.add(wordCA, wordCA.length);
		steemer.stem();
		return steemer.toString();
	}
}
