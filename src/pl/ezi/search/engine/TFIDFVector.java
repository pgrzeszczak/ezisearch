package pl.ezi.search.engine;

import java.util.ArrayList;

public class TFIDFVector {
	ArrayList<Double> list;
	
	public TFIDFVector(Keywords keywords, WordBag words, double idf[]) {
		list = new ArrayList<Double>();
		//wordbag
		for (int i=0; i<keywords.size(); i++) {
			list.add((double)words.getCount(keywords.get(i)));
		}
		//normalize
		double sum = 0;
		for (int i=0; i<list.size(); i++) {
			sum += list.get(i);
		}
		for (int i=0; i<list.size(); i++) {
			list.set(i, list.get(i) / sum);
		}
		//idf
		for (int i=0; i<list.size(); i++) {
			list.set(i, list.get(i) * idf[i]);
		}
	}
	
	public double vectorLength() {
		double sum = 0;
		for (Double number : list) {
			sum += Math.pow(number, 2);
		}
		return Math.sqrt(sum);
	}
	
	public double get(int index) {
		return list.get(index);
	}
	
	public double calcSIM(TFIDFVector vector) {
		double sum = 0;
		for (int i=0; i<list.size(); i++) {
			sum += get(i) * vector.get(i);
		}
		return sum / (vectorLength() * vector.vectorLength());
	}
}
