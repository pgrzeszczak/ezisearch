package pl.ezi.search.engine;

import java.util.Comparator;

public class CorrelationPair implements Comparable<CorrelationPair> {
	double value;
	int index;
	public CorrelationPair(double value, int index) {
		super();
		this.value = value;
		this.index = index;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	@Override
	public int compareTo(CorrelationPair o) {
		if (getValue() < o.getValue()) return 1;
		if (getValue() > o.getValue()) return -1;
		return 0;
	}
}
