package pl.ezi.search.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.ezi.search.Settings;

public class Documents {
	ArrayList<Document> list;
	Settings settings;
	
	public Documents(Settings settings)
	{
		list = new ArrayList<Document>();
		this.settings = settings;
	}
	
	public boolean readFromFile(File file) {
		list.clear();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			ArrayList<String> docLines = new ArrayList<String>();
			while ((line = br.readLine()) != null) {
				if (line.isEmpty()) {
					list.add(new Document(docLines, settings));
					docLines.clear();
				} else {
					docLines.add(line);
				}
			}
			if (!docLines.isEmpty()) {
				list.add(new Document(docLines, settings));
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Document get(int index) {
		return list.get(index);
	}
	
	public int size() {
		return list.size();
	}
	
	@Override
	public String toString() {
		String docs = "";
		for (Document doc : list) {
			docs += doc.toString() + "\n\n";
		}
		return docs;
	}
	
	public int countWithWord(String word) {
		int count = 0;
		for (Document doc : list) {
			if (doc.hasWord(word)) {
				count++;
			}
		}
		return count;
	}
	
	public List<Document> getAsRankList() {
		ArrayList<Document> rank = new ArrayList<Document>(list);
		Collections.sort(rank, Collections.reverseOrder());
		return rank;
	}
}
