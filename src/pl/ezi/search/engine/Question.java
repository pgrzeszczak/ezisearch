package pl.ezi.search.engine;

import java.util.ArrayList;
import java.util.List;

import pl.ezi.search.Settings;


public class Question {
	WordBunch wordBunch;
	String question;
	Settings settings;
	WordBunch expanded;
	
	public Question(String question, Settings settings) {
		this.question = question;
		this.settings = settings;
		wordBunch = new WordBunch(settings);
		wordBunch.readFromString(question);
		expanded = new WordBunch(settings);
	}
	public Question(String question, Settings settings, List<String> expanded) {
		this(question, settings);
		String expandedString = "";
		for (String exp : expanded) {
			expandedString += " " + exp;
		}
		this.expanded.readFromString(expandedString);
	}
	
	public WordBunch getWordBunch() {
		return wordBunch;
	}
	public WordBunch getExpanded() {
		return expanded;
	}
}
