package pl.ezi.search.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class WordBag {
	HashMap<String, Integer> map;
	ArrayList<String> uniqueWords;
	
	public WordBag(List<String> list) {
		map = new HashMap<String, Integer>();
		uniqueWords = new ArrayList<String>();
		for (String word : list) {
			if (map.containsKey(word)) {
				map.put(word, map.get(word) + 1);
			} else {
				map.put(word, 1);
				uniqueWords.add(word);
			}
		}
	}
	
	public int getCount(String word) {
		Integer count = map.get(word);
		return count == null ? 0 : count;
	}
	
	public int getCount(int index) {
		return map.get(getWord(index));
	}
	
	public int size() {
		return uniqueWords.size();
	}
	
	public String getWord(int index) {
		return uniqueWords.get(index);
	}
}
