package pl.ezi.search.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import pl.ezi.search.Settings;

public class Engine {
	
	double[][] correlation;
	Settings settings;
	
	public Engine(Settings settings) {
		super();
		this.settings = settings;
	}

	public List<Document> search(Documents documents, Keywords keywords, Question question) {
		double idf[] = recalcIdfWithExpanded(calcIDF(documents, keywords), question.getExpanded(), keywords);
		return search(documents, keywords, question, idf);
	}
	
	protected List<Document> search(Documents documents, Keywords keywords, Question question, double[] idf) {
		TFIDFVector questionTFIDF = new TFIDFVector(keywords, question.getWordBunch().getWordBag(), idf);
		TFIDFVector documentsTFIDF[] = new TFIDFVector[documents.size()];
		for (int i=0; i<documents.size(); i++) {
			documentsTFIDF[i] = new TFIDFVector(keywords, documents.get(i).getProcessedData().getWordBag(), idf);
		}
		for (int i=0; i<documents.size(); i++) {
			documents.get(i).setSIM(questionTFIDF.calcSIM(documentsTFIDF[i]));
		}
		return documents.getAsRankList();
	}
	
	protected double[] recalcIdfWithExpanded(double[] idf, WordBunch expanded, Keywords keywords) {
		for (int i=0; i<expanded.getWordBag().size(); i++) {
//			System.out.println(expanded.getWordBag().getWord(i) + " " + keywords.findIndex(expanded.getWordBag().getWord(i)));
			idf[keywords.findIndex(expanded.getWordBag().getWord(i))] *= settings.getExtensionWordsWeight();
		}
		return idf;
	}
	
	public List<String> getExpansionProposal(Documents documents, Keywords keywords, Question question) {
		ArrayList<String> proposal = new ArrayList<String>();
		ArrayList<String> stemmedProposal = new ArrayList<String>();
		calcCorrelation(documents, keywords);
		PriorityQueue<CorrelationPair> queue = new PriorityQueue<>();
		for (int i=0; i<question.getWordBunch().getWordBag().size(); i++) {
			String word = question.getWordBunch().getWordBag().getWord(i);
			int keywordIndex = keywords.findIndex(word);
			if (keywordIndex == -1) continue;
			for (int j=0; j<correlation[keywordIndex].length; j++) {
				queue.add(new CorrelationPair(correlation[keywordIndex][j], j));
			}
		}
		int howMany = settings.getExtensionWordsCount();
		while (howMany > 0) {
			CorrelationPair best = queue.poll();
			if (best == null) break;
			if (question.getWordBunch().has(keywords.get(best.index))) continue;
			if (stemmedProposal.contains(keywords.get(best.index))) continue;
			proposal.add(keywords.getOriginalWord(best.index));
			stemmedProposal.add(keywords.get(best.index));
//			System.out.println(best.value + " " + keywords.getOriginalWord(best.index) + " " + keywords.get(best.index));
			howMany--;
		}
		return proposal;
	}
	
	private void calcCorrelation(Documents documents, Keywords keywords) {
		correlation = new double[keywords.size()][keywords.size()];
		double[][] tmpCorrelation = new double[keywords.size()][keywords.size()];
		for (int i=0; i<keywords.size(); i++) {
			for (int j=0; j<keywords.size(); j++) {
				double sum = 0;
				for (int k=0; k<documents.size(); k++) {
					sum += documents.get(k).getWordFrequent(keywords.get(i)) * documents.get(k).getWordFrequent(keywords.get(j));
				}
				tmpCorrelation[i][j] = sum;
			}
		}
		//normalize
		for (int i=0; i<keywords.size(); i++) {
			for (int j=0; j<keywords.size(); j++) {
				correlation[i][j] = tmpCorrelation[i][j] / (tmpCorrelation[i][i] + tmpCorrelation[j][j] - tmpCorrelation[i][j]);
//				System.out.println(i + " " + keywords.get(i) + " " + j + " " + keywords.get(j) + " " + correlation[i][j]);
			}
		}
	}
	
	private double[] calcIDF(Documents documents, Keywords keywords) {
		double idf[] = new double[keywords.size()];
		for (int i=0; i<keywords.size(); i++) {
			String word = keywords.get(i);
			double countWithWord = (double)documents.countWithWord(word);
			idf[i] = countWithWord > 0 ? Math.log((double)documents.size() / countWithWord) : 0;
		}
		return idf;
	}
}
