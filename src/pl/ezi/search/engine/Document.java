package pl.ezi.search.engine;

import java.util.ArrayList;

import pl.ezi.search.Settings;

public class Document implements Comparable<Document> {
	private String title = "";
	private String content = "";
	private WordBunch processedData;
	private double finalSIM = 0;
	private Settings settings;
	public Document(ArrayList<String> lines, Settings settings) {
		this.settings = settings;
		title = lines.get(0);
		for (int i=1; i<lines.size(); i++) {
			content += lines.get(i) + (i==lines.size()-1 ? "" : "\n");
		}
		String dataToProcess = content;
		if (settings.isUseTitleAsContent()) {
			dataToProcess = title + "\n" + content;
		}
		processedData = createDataFromString(dataToProcess);
	}

	public String getTitle() {
		return title;
	}
	public String getContent() {
		return content;
	}
	
	@Override
	public String toString() {
		return processedData.toString();
	}
	
	public WordBunch getProcessedData() {
		return processedData;
	}
	
	public boolean hasWord(String word) {
		return processedData.has(word);
	}
	
	public double getSIM() {
		return finalSIM;
	}

	public void setSIM(double SIM) {
		finalSIM = SIM;
	}
	
	public double getWordFrequent(String word) {
		return processedData.getWordFrequent(word);
	}

	private WordBunch createDataFromString(String rawData) {
		WordBunch data = new WordBunch(settings);
		data.readFromString(rawData);
		return data;
	}

	@Override
	public int compareTo(Document o) {
		if (getSIM() < o.getSIM()) return -1;
		if (getSIM() > o.getSIM()) return 1;
		return 0;
	}
}
