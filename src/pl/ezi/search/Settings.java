package pl.ezi.search;

import java.io.File;

public class Settings {
	boolean stemming;
	boolean useTitleAsContent;
	File documentsFile;
	File keywordsFile;
	int extensionWordsCount;
	double extensionWordsWeight;
	public Settings(boolean stemming, boolean useTitleAsContent,
			int extensionWordsCount) {
		super();
		this.stemming = stemming;
		this.useTitleAsContent = useTitleAsContent;
		this.extensionWordsCount = extensionWordsCount;
		extensionWordsWeight = 1;
	}
	public double getExtensionWordsWeight() {
		return extensionWordsWeight;
	}
	public void setExtensionWordsWeight(double extensionWordsWeight) {
		if (extensionWordsWeight > 1) extensionWordsWeight = 1;
		if (extensionWordsWeight < 0) extensionWordsWeight = 0;
		this.extensionWordsWeight = extensionWordsWeight;
	}
	public boolean isStemming() {
		return stemming;
	}
	public void setStemming(boolean stemming) {
		this.stemming = stemming;
	}
	public boolean isUseTitleAsContent() {
		return useTitleAsContent;
	}
	public void setUseTitleAsContent(boolean useTitleAsContent) {
		this.useTitleAsContent = useTitleAsContent;
	}
	public File getDocumentsFile() {
		return documentsFile;
	}
	public void setDocumentsFile(File documentsFile) {
		this.documentsFile = documentsFile;
	}
	public File getKeywordsFile() {
		return keywordsFile;
	}
	public void setKeywordsFile(File keywordsFile) {
		this.keywordsFile = keywordsFile;
	}
	public int getExtensionWordsCount() {
		return extensionWordsCount;
	}
	public void setExtensionWordsCount(int extensionWordsCount) {
		this.extensionWordsCount = extensionWordsCount;
	}
}
