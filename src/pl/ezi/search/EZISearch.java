package pl.ezi.search;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import pl.ezi.search.engine.Document;
import pl.ezi.search.engine.Documents;
import pl.ezi.search.engine.Engine;
import pl.ezi.search.engine.Keywords;
import pl.ezi.search.engine.Question;


public class EZISearch {
	
	JFrame mainFrame;
	JTextArea rawDocumentsTextArea;
	JPanel searchResultsPanel;
	JScrollPane searchResultsScrollPane;
	JButton expandButton;
	Keywords keywords;
	Documents documents;
	Engine engine;
	Settings settings;
	Integer[] availableQueryExtensions = {0, 1, 2, 3, 4, 5};

	public EZISearch() {
		prepareMainFrame();
		settings = new Settings(true, true, 0);
		keywords = new Keywords(settings);
		documents = new Documents(settings);
		engine = new Engine(settings);
	}
	
	public void run() {
		mainFrame.setVisible(true);
	}
	
	public static void main(String[] args) {
		EZISearch app = new EZISearch();
		app.run();
	}
	
	protected void reloadFiles(boolean reloadDocuments, boolean reloadKeywords) {
		if (reloadDocuments && settings.getDocumentsFile() != null) {
			documents.readFromFile(settings.getDocumentsFile());
			rawDocumentsTextArea.setText(documents.toString());
		}
		if (reloadKeywords && settings.getKeywordsFile() != null) {
			keywords.readFromFile(settings.getKeywordsFile());
		}
	}
	
	protected void reloadFiles() {
		reloadFiles(true, true);
	}
	
	protected void reloadDocuments() {
		reloadFiles(true, false);
	}
	
	protected void reloadKeywords() {
		reloadFiles(false, true);
	}

	private void prepareMainFrame() {
		mainFrame = new JFrame("EZI Search");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(900, 600);
		mainFrame.setResizable(false);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.add(createSettingsPanel(), BorderLayout.NORTH);
		mainFrame.add(createRawDocumentsPanel(), BorderLayout.WEST);
		mainFrame.add(createSearchMainPanel(), BorderLayout.CENTER);
	}
	
	private JPanel createSettingsPanel() {
		JPanel settingsPanel = new JPanel(new GridLayout(1, 6));
		settingsPanel.setBorder(BorderFactory.createTitledBorder("Ustawienia"));
		settingsPanel.add(createChooseDocumentsPanel());
		settingsPanel.add(createChooseKeywordsPanel());
		final JCheckBox stemmingCheckbox = new JCheckBox("Stemming");
		stemmingCheckbox.setSelected(true);
		stemmingCheckbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				settings.setStemming(stemmingCheckbox.isSelected());
				reloadFiles();
			}
		});
		settingsPanel.add(stemmingCheckbox);
		final JCheckBox titleCheckbox = new JCheckBox("Szukaj w tytu�ach");
		titleCheckbox.setSelected(true);
		titleCheckbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				settings.setUseTitleAsContent(titleCheckbox.isSelected());
				reloadFiles();
			}
		});
		settingsPanel.add(titleCheckbox);
		
		settingsPanel.add(createExtensionQueryCountPanel());
		settingsPanel.add(createExtensionQueryWeightPanel());
		return settingsPanel;
	}
	
	private JPanel createExtensionQueryCountPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		final JLabel label = new JLabel("Roszerzanie o s�owa:");
		final JComboBox<Integer> extensionList = new JComboBox<Integer>(availableQueryExtensions);
		extensionList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				settings.setExtensionWordsCount((Integer)extensionList.getSelectedItem());
				expandButton.setEnabled(settings.getExtensionWordsCount() != 0);
			}
		});
		panel.add(label, BorderLayout.NORTH);
		panel.add(extensionList, BorderLayout.CENTER);
		return panel;
	}
	
	private JPanel createExtensionQueryWeightPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		final JLabel label = new JLabel("Waga rozszerzenia:");
		final JTextField field = new JTextField();
		field.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				rewrite();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				rewrite();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				rewrite();
			}
			private void rewrite() {
				try {
					settings.setExtensionWordsWeight(Double.valueOf(field.getText()));
				} catch (Exception ex) {
					settings.setExtensionWordsWeight(1.0);
				}
//				System.out.println(settings.getExtensionWordsWeight());
			}
		});
		panel.add(label, BorderLayout.NORTH);
		panel.add(field, BorderLayout.CENTER);
		return panel;
	}
	
	private JPanel createChooseDocumentsPanel() {
		JPanel chooseDocumentsPanel = new JPanel();
		chooseDocumentsPanel.setLayout(new BoxLayout(chooseDocumentsPanel, BoxLayout.Y_AXIS));
		final JLabel chooseDocumentsLabel = new JLabel("documents");
		chooseDocumentsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JButton chooseDocumentsButton = new JButton("Wybierz plik dokument�w");
		chooseDocumentsButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		chooseDocumentsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				int val = fc.showOpenDialog(mainFrame);
				if (val == JFileChooser.APPROVE_OPTION) {
					settings.setDocumentsFile(fc.getSelectedFile());
					reloadDocuments();
					chooseDocumentsLabel.setText(fc.getSelectedFile().getName());
				}
			}
		});
		chooseDocumentsPanel.add(chooseDocumentsLabel);
		chooseDocumentsPanel.add(chooseDocumentsButton);
		return chooseDocumentsPanel;
	}
	
	private JPanel createChooseKeywordsPanel() {
		JPanel chooseKeywordsPanel = new JPanel();
		chooseKeywordsPanel.setLayout(new BoxLayout(chooseKeywordsPanel, BoxLayout.Y_AXIS));
		final JLabel chooseKeywordsLabel = new JLabel("keywords");
		chooseKeywordsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JButton chooseKeywordsButton = new JButton("Wybierz plik s��w kluczowych");
		chooseKeywordsButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		chooseKeywordsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				int val = fc.showOpenDialog(mainFrame);
				if (val == JFileChooser.APPROVE_OPTION) {
					settings.setKeywordsFile(fc.getSelectedFile());
					reloadKeywords();
					chooseKeywordsLabel.setText(fc.getSelectedFile().getName());
				}
			}
		});
		chooseKeywordsPanel.add(chooseKeywordsLabel);
		chooseKeywordsPanel.add(chooseKeywordsButton);
		return chooseKeywordsPanel;
	}
	
	private JPanel createRawDocumentsPanel() {
		JPanel rawDocumentsPanel = new JPanel(new BorderLayout());
		rawDocumentsPanel.setPreferredSize(new Dimension(300, 300));
		rawDocumentsPanel.setBorder(BorderFactory.createTitledBorder("Dane"));
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		rawDocumentsTextArea = new JTextArea();
		rawDocumentsTextArea.setEditable(false);
		rawDocumentsTextArea.setLineWrap(true);
		rawDocumentsTextArea.setWrapStyleWord(true);
		scrollPane.setViewportView(rawDocumentsTextArea);
		rawDocumentsPanel.add(scrollPane, BorderLayout.CENTER);
		return rawDocumentsPanel;
	}
	
	private JPanel createSearchMainPanel() {
		JPanel rawSearchMainPanel = new JPanel(new BorderLayout());
		rawSearchMainPanel.setBorder(BorderFactory.createTitledBorder("Wyszukiwarka"));
		rawSearchMainPanel.add(createSearchInputPanel(), BorderLayout.NORTH);
		rawSearchMainPanel.add(createSearchResultsPanel(), BorderLayout.CENTER);
		return rawSearchMainPanel;
	}
	
	private JPanel createSearchInputPanel() {
		final JPanel rawSearchInputPanel = new JPanel(new BorderLayout());
		final JTextField searchTextField = new JTextField();
		
		JPanel panel = new JPanel(new GridLayout(1, 2));
		JButton searchButton = new JButton("Szukaj");
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!searchTextField.getText().isEmpty()) {
					Question question = new Question(searchTextField.getText(), settings);
					List<Document> rankList = engine.search(documents, keywords, question);
					searchResultsPanel.removeAll();
					for (int i=0; i<rankList.size(); i++) {
						searchResultsPanel.add(createSimpleResultPanel(rankList.get(i), i+1));
//						System.out.println(doc.getSIM());// + " " + ++i + " " + documents.size());
//						System.out.println(doc.getTitle());
//						System.out.println(doc.getContent());
					}
					SwingUtilities.invokeLater(new Runnable() {
					   public void run() { 
						   searchResultsScrollPane.getVerticalScrollBar().setValue(0);
						   searchResultsPanel.revalidate();
					   }
					});
				}
			}
		});
		panel.add(searchButton);
		expandButton = new JButton("Rozszerz i szukaj");
		expandButton.setEnabled(false);
		expandButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!searchTextField.getText().isEmpty()) {
					Question question = new Question(searchTextField.getText(), settings);
					List<String> proposal = engine.getExpansionProposal(documents, keywords, question);
					String expandedString = searchTextField.getText();
					for (String prop : proposal) {
						expandedString += " " + prop;
					}
					searchTextField.setText(expandedString);
					
					Question expandedQuestion = new Question(searchTextField.getText(), settings, proposal);
					List<Document> rankList = engine.search(documents, keywords, expandedQuestion);
					searchResultsPanel.removeAll();
					for (int i=0; i<rankList.size(); i++) {
						searchResultsPanel.add(createSimpleResultPanel(rankList.get(i), i+1));
					}
					
					SwingUtilities.invokeLater(new Runnable() {
					   public void run() { 
						   searchResultsScrollPane.getVerticalScrollBar().setValue(0);
							searchResultsPanel.revalidate();
							rawSearchInputPanel.revalidate();
					   }
					});
				}
			}
		});
		panel.add(expandButton);
		rawSearchInputPanel.add(panel, BorderLayout.EAST);
		rawSearchInputPanel.add(searchTextField, BorderLayout.CENTER);
		return rawSearchInputPanel;
	}
	
	
	private JScrollPane createSearchResultsPanel() {
		searchResultsPanel = new JPanel();
		searchResultsPanel.setLayout(new BoxLayout(searchResultsPanel, BoxLayout.Y_AXIS));
		searchResultsPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		searchResultsScrollPane = new JScrollPane(searchResultsPanel);
		return searchResultsScrollPane;
	}
	
	private JPanel createSimpleResultPanel(Document document, int rank) {
		JPanel resultPanel = new JPanel(new BorderLayout());
		resultPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 10, 10, 10), new EtchedBorder()));
		JLabel title = new JLabel(rank + ". - " + (double)Math.round(document.getSIM()*10000)/10000.0 + " - " +  document.getTitle());
		JTextArea content = new JTextArea(document.getContent());
		content.setEditable(false);
		content.setLineWrap(true);
		content.setWrapStyleWord(true);
		resultPanel.add(title, BorderLayout.NORTH);
		resultPanel.add(content, BorderLayout.CENTER);
		return resultPanel;
	}
}
